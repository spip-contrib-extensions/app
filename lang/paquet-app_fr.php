<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
'app_description' => 'Ce plugin est un outil permettant de créer des API de lecture/écriture pour vos contenus depuis des applications tierces, en implémentant un serveur APP (Atom Publishing Protocol).',
'app_slogan' => 'Protocole de publication Atom',
);

?>
